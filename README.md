# This is the NuCamp React-Native Course. 

This course builds on the React course of NuCamp. It uses React Native that allow to create native apps for Android and IOS using React.

## This is using the json-server for the backup to host the Campground Informaiton. 

This app need to have you local ip address in the /shared/baseurl.js file to run the App.

### Here is the command that NuCamp wanted to students to use for the Json-server

json-server -H 0.0.0.0 --watch db.json -p 3001 -d 2000

## The Main Technologies is using:
- React 16.3.1
- React-Native SDK-41.0.0
- React Navigation 4
- Redux 4.0.5
- Expo 41

## Home Page
The App will first start with a Loading icons until it retrieves the campground information.

<img src="/ScreenShots/loading.jpg" alt="Loading"
    title="Loading" width="360" height="740" />
    
The App loads two campgrounds and node apparel info.

<img src="/ScreenShots/home.jpg" alt="Home Page"
    title="Home Page" width="360" height="740" />


## React-Native Navigation
This is the React Navigation Drawer

<img src="/ScreenShots/navigation.jpg" alt="Navigation Drawer"
    title="Navigation Drawer" width="360" height="740" />

## The login
You can enter you user name and password

<img src="/ScreenShots/login.jpg" alt="Log In Page"
    title="Log In Page" width="360" height="740" />

## The Register Page
- Enter you information to register.
- You can even take a the camera to take a picture for your avatar.
- You even can use Gallary to select a picture for your avator. 

<img src="/ScreenShots/signup.jpg" alt="Register Page"
    title="Register Page" width="360" height="740" />

## The Directory Page.
The Directory Page Show all the campgrounds you can go to.

<img src="/ScreenShots/directory.jpg" alt="Directory Page"
    title="Directory Page" width="360" height="740" />

### Click on a campground in the directory page
- Information on the Campground
- Show peoples comments

<img src="/ScreenShots/information.jpg" alt="Information Page"
    title="Informaiton Page" width="360" height="740" />
- You can leave a comment about that campground

<img src="/ScreenShots/leaveacomment.jpg" alt="Leave A Comment"
    title="Leave A Comment" width="360" height="740" />

- you can added to your favorites.

## The Reserve Campsite
The Page allow you to reserve a campground.

<img src="/ScreenShots/reserve.jpg" alt="Reserve Page"
    title="Reserve Page" width="360" height="740" />

## My Favorites Page
This Page you show your favorites and allow you to delete them with a swape and click. 

<img src="/ScreenShots/swape.jpg" alt="Favorites Page"
    title="Favorites Page" width="360" height="740" />

## The About Page
This page tells you about the compnay mission and their Partners.

<img src="/ScreenShots/about.jpg" alt="About Page"
    title="About Page" width="360" height="740" />

## Contact Us Page
The page will allow people to send email to the company. 

<img src="/ScreenShots/contact.jpg" alt="Contact Page"
    title="Contact Page" width="360" height="740" />

